using System;

namespace AnagramTask
{
    public class Anagram
    {
        private readonly string sourceWord;

        public Anagram(string sourceWord)
        {
            if (sourceWord is null)
            {
                throw new ArgumentNullException(nameof(sourceWord));
            }

            if (string.IsNullOrEmpty(sourceWord))
            {
                throw new ArgumentException("String is empty.", nameof(sourceWord));
            }

            this.sourceWord = sourceWord;
        }

        public string[] FindAnagrams(string[] candidates)
        {
            if (candidates is null)
            {
                throw new ArgumentNullException(nameof(candidates));
            }

            string[] results = Array.Empty<string>();
            int currentIndex = 0;

            char[] convWord = this.sourceWord.ToUpperInvariant().ToCharArray();

            foreach (var word in candidates)
            {
                if (this.sourceWord.Length == word.Length && this.sourceWord.ToUpperInvariant() != word.ToUpperInvariant())
                {
                    int matchCounter = convWord.Length;
                    string[] tempResults;
                    string tempWord = word;

                    for (int i = 0; i < convWord.Length; i++)
                    {
                        if (i + matchCounter != convWord.Length)
                        {
                            break;
                        }
                        else
                        {
                            char[] letters = tempWord.ToUpperInvariant().ToCharArray();

                            for (int j = 0; j < letters.Length; j++)
                            {
                                if (convWord[i] == letters[j])
                                {
                                    matchCounter--;
                                    tempWord = tempWord.Remove(j, 1);
                                    break;
                                }
                            }
                        }                       
                    }

                    if (matchCounter == 0 && currentIndex == 0)
                    {
                        currentIndex++;
                        results = new string[currentIndex];
                        results[0] = word;
                    }
                    else if (matchCounter == 0 && currentIndex > 0)
                    {
                        currentIndex++;
                        tempResults = new string[currentIndex];

                        for (int i = 0; i < results.Length; i++)
                        {
                            tempResults[i] = results[i];
                        }

                        for (int i = results.Length; i < tempResults.Length; i++)
                        {
                            tempResults[i] = word;
                        }

                        results = tempResults;
                    }
                }
            }

            return results;
        }
    }
}